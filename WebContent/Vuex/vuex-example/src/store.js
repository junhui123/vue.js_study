import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export const store = new Vuex.Store({
	state: {
		counter: 0
	},
	getters: {
		getCounter(state) {
			return state.counter
		}
	},
	mutations: {
		addCounter(state, payload) {
			return state.counter++;
		},
		payloadCounter(state, payload) {
			//console.log(payload.value)
			//console.log(payload.arr)
			state.counter = payload.value
		},
		increment(state, payload){
			state.counter = payload
		}
	},
	actions: {
		addCounter(context) {
			debugger
			//mutations의 addCounter를 의미. 상태 변화 추적을 위함
			return context.commit('addCounter')
		},
		//http request 혹은 setTimeout과 같은 비동기 처리 로직의 사용
		getServerData(context){
			return axios.get("...").then(function(){
				//...
			})
		},
		delay(context){
			return setTimeout(function(){
				commit('addCounter')
			}, 1000)
		},
		asyncIncrement(context, payload){
			debugger
			return setTimeout(function(){
				context.commit('increment', payload.by)
			}, payload.duration)
		}
	}
})
