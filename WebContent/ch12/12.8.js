Vue.component('movie' , {
	template: '#template-movie-raw',
	props: ['movie'],
	methods: {
		editMovie: function(movie) {
			movie.editing = true;
		},
		deleteMovie: function(movie) {
			var index = vm.movies.indexOf(movie);
			vm.movies.splice(index, 1);
			axios.delete('/api/movies/'+movie.id);
		},
		updateMovie: function(movie) {
			axios.patch('/api/movies/'+movie.id, movie);
			movie.editing = false;
		},
		storeMovie: function(movie) {
			axios.post('/api/movies/', movie).then(function (response) {
				Vue.set(movie, 'id', response.data.id);
				movie.editing=false;
			});
		}
	}
});

var vm = new Vue({
	el: '#v-app',
	data: {
		movies: [],
		movie: {}
	},
	mounted: function() { 
		this.fetchMovies();
	},
	methods: {
		createMovie: function() {
			var newMovie = {
				title: "",
				director: "",
				editing: true
			}
			this.movies.push(newMovie);
		},
		fetchMovies: function(){
			//var vm = this;
			axios.get('/api/movies')
				.then(function(response){
					var moviesReady = response.data.map(function(movie) {
						movie.editing=false;
						return movie;
					})
				Vue.set(vm, 'movies', moviesReady);
			});
		}
	}
});