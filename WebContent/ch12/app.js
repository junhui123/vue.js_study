Vue.component('story' , {
	template: '#template-story-raw',
	props: ['story'],
	methods: {
		deleteStory: function(story) {
			//var index = this.$parent.stories.indexOf(story);
			//this.$parent.stories.splice(index, 1);
			var index = vi.stories.indexOf(story);
			vi.stories.splice(index, 1);
			this.$http.delete('/api/stories/'+story.id);
		},
		upvoteStory: function(story){
			story.upvotes++;
			this.$http.patch('/api/stories/'+story.id, story);
		},
		editStory: function(story) {
			story.editing = true;
		},
		updateStory: function(story) {
			this.$http.patch('/api/stories/'+story.id, story);
			story.editing = false;
		},
		storeStory: function(story) {
			this.$http.post('/api/stories', story).then(function(response) {
				//createStory()에서 만든 newStory는 id가 지정되어 있지 않음
				//id 속성은 db에서 자동 증분으로 설정됨. 
				//response를 통해서 생성된 id를 받아 설정 필요.
				//새로운 속성이 추가되는것이므로 반응형으로 만들어 업데이트를 위해 Vue.set 사용
				Vue.set(story, 'id', response.data.id);
				story.editing=false;
			});
		}
	}
});

var vi = new Vue({
	el: "#v-app",
	data: {
		stories: [],
		story: {}
	},
	mounted: function(){
		//vi.fetchStories()
		//mounted 시점에는 변수명으로 접근 불가. this를 사용
		//this == 현재 Vue 인스턴스를 가리킴
		this.fetchStories();
	},
	methods: {
		createStory: function() {
			var newStory = {
				plot: "",
				upvotes: 0,
				editing: true
			};
			this.stories.push(newStory);
		},
		fetchStories: function() {
			var vm = this;
			vm.$http.get('/api/stories')
				//데이터가 변경되면 화면도 다시 렌더링 되기 위함
				.then(function(response){
					var storiesReady = response.data.map(function(story){
					story.editing = false;
					return story;
					})
					//새로운 속성인 editing이 추가/삭제 되는 경우 DOM이 수정되지 않음
					//런타임에 추가/삭제 시 Vue.set/Vue.delete로  반응형 속성으로 만들고 뷰 업데이트 발생
					Vue.set(vm, 'stories', storiesReady);
				}) ;
		}
	}
});