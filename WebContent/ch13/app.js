Vue.component('story' , {
	template: '#template-story-raw',
	props: ['story'],
	methods: {
		deleteStory: function(story) {
			var index = vi.stories.indexOf(story);
			vi.stories.splice(index, 1);
			this.$http.delete('/api/stories/'+story.id);
		},
		upvoteStory: function(story){
			story.upvotes++;
			this.$http.patch('/api/stories/'+story.id, story);
		},
		editStory: function(story) {
			story.editing = true;
		},
		updateStory: function(story) {
			this.$http.patch('/api/stories/'+story.id, story);
			story.editing = false;
		},
		storeStory: function(story) {
			this.$http.post('/api/stories', story).then(function(response) {
				Vue.set(story, 'id', response.data.id);
				story.editing=false;
			});
		}
	}
});

var vi = new Vue({
	el: "#v-app",
	data: {
		stories: [],
		pagination: {},
		story: {}
	},
	mounted: function(){
		this.fetchStories();
	},
	methods: {
		createStory: function() {
			var newStory = {
				plot: "",
				upvotes: 0,
				editing: true
			};
			this.stories.push(newStory);
		},
		//bad...
		//fetchStories: function(page_url = '/api/stories') {
			//page_url = page_url || '/api/stories';
		fetchStories: function(page_url = '/api/stories') {
			var vm = this;
			vm.$http.get(page_url)
				.then(function(response){
					var storiesReady = response.data.data.map(function(story){
						story.editing = false;
						return story;
					})
					vm.makePagination(response.data);
					Vue.set(vm, 'stories', storiesReady);
				}) ;
		},
		makePagination: function(data){
			var pagination = {
				current_page: data.current_page,
				last_page: data.last_page,
				next_page_url: data.next_page_url,
				prev_page_url: data.prev_page_url
			};
			Vue.set(vi, 'pagination', pagination);
		}
	}
});