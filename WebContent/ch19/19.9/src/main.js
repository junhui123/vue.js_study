import Vue from 'vue'
import App from './App.vue'
import VueRouter from 'vue-router'
import home from './components/home'
import category from './components/category'
import create from './components/create'

Vue.use(VueRouter);

const routes = [
  {
  	path: '/',
  	name: 'home',
  	component: home
  },
  {
	path: '/category/:name',
	name: 'show',
	component: category,
	children: [
		{
		 path: 'pokemons/new',
		 component: create
		}
	]
  }
]

const router = new VueRouter({
	mode: 'history',
	base: '/',
	routes	
})

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  template: '<app></app>',
  components: {
	  App
  }
})