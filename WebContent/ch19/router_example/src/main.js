import Vue from 'vue'
import App from './App.vue'
import VueRouter from 'vue-router'
import Hello from './components/Hello.vue'
import Login from './components/Login.vue'
import StoriesPage from './components/StoriesPage'
import StoriesAll from './components/StoriesAll'
import StoriesFamous from './components/StoriesFamous'
import StoriesEdit from './components/StoriesEdit'

//Vue Router 플러그인 사용
Vue.use(VueRouter);

const routes = [
	{
		path: '/',
		name: 'home',
		component: Hello
	},
	{
		path: '/login',
		name: 'login',
		component: Login
	},
	{
		path: '/stories',
		component: StoriesPage,
		children:[
			{
				path: '',
				name: 'stories.all',
				component: StoriesAll
			},
			{
				path: 'famous',
				name: 'stories.famous',
				// '/stories/famous'를 '/famous'로 접근
				alias: '/famous',
				component: StoriesFamous
			},
			{
				//:id == 동적 세그먼트. 값이 변경
				path: ':id/edit',
				props: (route)=>({id: Number(route.params.id)}),
				name: 'stories.edit',
				component: StoriesEdit
			}
		]
	}
];

//router는 routes 코드상 아래 위치에 존재 해야함
//routes 보다 위에 존재한다면 Route with name '~~' does not exist 오류 발생
const router = new VueRouter({
	mode: 'history',
	base: '/',
	linkActiveClass: 'my-active-class',
	routes
});

let User = {
	isAdmin: false
}

router.beforeEach((to, from, next)=> {
	if(to.path != '/login' && !User.isAdmin){
		next('/login')
	} else {
		next()
	}
})

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  template: '<app></app>',
  components: {
	  App
  }
})
